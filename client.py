from game import ChessGame, PIECE_EMPTY, ChessPieceType, ChessPlayer
from server import TYPE_FORMAT, TYPE_CONFIG, TYPE_MOVE, TYPE_PROMOTE, TYPE_HANDSHAKE, TYPE_OPPONENT_FOUND, \
    TYPE_OPPONENT_DISCONNECTED, MOVE_FORMAT, PROMOTE_FORMAT, PLAYER_FORMAT, CONFIG_FORMAT, WELCOME_DATA, TYPE_CUSTOM, \
    TYPE_CLOCK_SYNC, SYNC_FORMAT, TYPE_GAME_END
from threading import Thread
from select import select
from time import sleep
from struct import pack, unpack, calcsize
import tkinter as tk
import tkinter.messagebox as tkmsgbox
import tkinter.font as tkfont
import socket
import base64
import gzip
import io
import sys

# TODO Offer draw and resign


def debug_print(mess, level=2):
    if debug >= level:
        print({2: "[INFO] ", 3: "[DEBUG] "}.get(level) + mess)


def redraw_board():
    debug_print('Redrawing board', 3)

    canvas.delete('image')
    canvas.delete('checkerboard')
    canvas.delete('animation')

    for x in range(8):
        for y in range(8):
            color = 'light gray' if (x + y) % 2 == (view - 1) else 'dark gray'
            create_tile((x, y), color, 'checkerboard')
            create_piece((x, y))


def redraw_selection():
    debug_print('Redrawing selection', 3)

    canvas.delete('selection')

    if selection is not None:
        create_tile(selection, '#ce4523', 'selection')
        for pos in possible_moves:
            create_tile(pos, '#e5c759', 'selection')

    canvas.tag_raise('image')


def create_piece(pos):
    uw, uh = W / 8., H / 8.
    x, y = pos
    tile = g.board[7 - y if view == 1 else y][x]
    if tile.piece.value > 0:
        t_i = (tile.piece.value - 1) * 2 + (tile.player.value - 1)
        canvas.create_image((uw * (x + 0.5), uh * (y + 0.5)), image=textures[t_i], tags=('image', 'image_{}x{}'.format(x, y)))


def create_tile(pos, color, tag):
    uw, uh = W / 8, H / 8
    x, y = pos
    canvas.create_rectangle(uw * x, uh * y, uw * (x + 1), uh * (y + 1), outline='gray', fill=color, tags=tag)


def animate_move(fro, to):
    debug_print('Animating move {} -> {}'.format(fro, to), 3)
    uw, uh = W / 8., H / 8.

    bfro = (fro[1], 7 - fro[0] if view == 1 else fro[0])
    canvas.delete('image_{}x{}'.format(*bfro))
    if to is None:
        return

    bto = (to[1], 7 - to[0] if view == 1 else to[0])
    canvas.delete('image_{}x{}'.format(*bto))

    diff_x, diff_y = bto[0] - bfro[0], bto[1] - bfro[1]
    dist_x, dist_y = diff_x * uw, diff_y * uh

    x, y = bfro
    tile = g.board[to[0]][to[1]]

    t_i = (tile.piece.value - 1) * 2 + (tile.player.value - 1)
    piece = canvas.create_image((uw * (x + 0.5), uh * (y + 0.5)), image=textures[t_i], tags=('animation', 'animation_{}x{}'.format(*bto)))

    ANIM_PRECISION = 10
    ANIM_TIME = 1.
    for _ in range(ANIM_PRECISION):
        canvas.move(piece, dist_x / ANIM_PRECISION, dist_y / ANIM_PRECISION)
        sleep(ANIM_TIME / ANIM_PRECISION)

    canvas.delete(piece)
    create_piece(bto)


def handle_click(event):
    global selection, selection_board, possible_moves, possible_moves_board, promotion_dialog

    half_pos = (event.x // (W // 16), event.y // (H // 16))
    pos = (half_pos[0] // 2, half_pos[1] // 2)
    debug_print('Board clicked at position {}'.format(pos), 3)
    board_pos = (7 - pos[1] if view == 1 else pos[1], pos[0])

    if promotion_dialog is not None:
        if pos == promotion_dialog:
            quadrant = (half_pos[0] - pos[0] * 2, half_pos[1] - pos[1] * 2)
            debug_print('Promotion dialog clicked in quadrant {}'.format(quadrant), 3)
            if quadrant == (0, 0):
                try_promote(player, board_pos, ChessPieceType.QUEEN)
            elif quadrant == (0, 1):
                try_promote(player, board_pos, ChessPieceType.BISHOP)
            elif quadrant == (1, 0):
                try_promote(player, board_pos, ChessPieceType.ROOK)
            elif quadrant == (1, 1):
                try_promote(player, board_pos, ChessPieceType.KNIGHT)
            canvas.delete('promotion')
            promotion_dialog = None
        return

    if g.get_turn() is not ChessPlayer(player):
        return

    if selection is None:
        if g.board[board_pos[0]][board_pos[1]].player.value == player:
            selection = pos
            selection_board = board_pos
            possible_moves_board = g.get_possible_moves(player, board_pos)
            possible_moves = [(m[1], 7 - m[0] if view == 1 else m[0]) for m in possible_moves_board]
    elif possible_moves_board is not None and board_pos in possible_moves_board:
        try_move(player, selection_board, board_pos)
        selection = None
    else:
        selection = None

    redraw_selection()


def show_promotion_dialog(pos):
    global promotion_dialog
    debug_print('Showing promotion dialog for position {}'.format(pos))
    uw, uh = W / 8., H / 8.

    board_pos = (pos[1], 7 - pos[0] if view == 1 else pos[0])
    canvas.delete('image_{}x{}'.format(*board_pos))
    x, y = board_pos
    promotion_dialog = board_pos

    for texoffset, xoffset, yoffset in ((0, 0.25, 0.25), (1, 0.75, 0.25), (2, 0.25, 0.75), (3, 0.75, 0.75)):
        i = textures_promotion[texoffset * 2 + player - 1]
        canvas.create_image((uw * (x + xoffset), uh * (y + yoffset)), image=i, tags='promotion')


def try_move(plr, fro, to):
    debug_print('Trying to move: {} {} {}'.format(fro, to, plr))
    sock.send(pack(TYPE_FORMAT, TYPE_MOVE) + pack(MOVE_FORMAT, plr, *fro, *to))
    # No response handling here - after allowing the server duplicates move message handled in handle_socket_recv


def try_promote(plr, pos, piece):
    debug_print('Trying to promote: {} {} {}'.format(pos, piece, plr))
    sock.send(pack(TYPE_FORMAT, TYPE_PROMOTE) + pack(PROMOTE_FORMAT, plr, *pos, piece.value))


def move(plr, fro, to):
    debug_print('Performing move: {} {} {}'.format(fro, to, plr))

    oldb = [r[:] for r in g.board]
    g.move(plr, fro, to)
    newb = g.board

    diff = tuple((x, y) for x in range(8) for y in range(8) if oldb[x][y] != newb[x][y])
    mv_fro = tuple(p for p in diff if newb[p[0]][p[1]] is PIECE_EMPTY)
    mv_to = tuple(p for p in diff if oldb[p[0]][p[1]] is not newb[p[0]][p[1]])

    for fro in mv_fro:
        toa = tuple(to for to in mv_to if oldb[fro[0]][fro[1]] is newb[to[0]][to[1]])

        if len(toa) == 0 or (toa[0] == to and plr == player and g.can_promote(plr, to)):
            anim = Thread(target=animate_move, args=(fro, None,))
        else:
            # Unexpected results if len(to) > 1
            anim = Thread(target=animate_move, args=(fro, toa[0],))

        anim.start()

    if plr == player and g.can_promote(plr, to):
        show_promotion_dialog(to)

    update_turn_display()
    check_game_end()

def check_game_end():
    if g.get_player_win() is not None:
        player_win(g.get_player_win())

    if g.get_draw() is not None:
        draw(g.get_draw())

def update_timers(clocks):
    time_var1.set(clocks[1 if view == 1 else 0])
    time_var2.set(clocks[0 if view == 1 else 1])
    check_game_end()

def promote(plr, pos, piece):
    debug_print('Performing promotion: {} {} {}'.format(pos, piece, plr))
    g.promote(plr, pos, piece)
    board_pos = pos[1], 7 - pos[0] if view == 1 else pos[0]
    canvas.delete('image_{}x{}'.format(*board_pos))
    create_piece(board_pos)


def initialize_textures():
    global textures, textures_promotion
    b64_data = """H4sIAEjdWFsAA62TUQ6AIAxDe/9Lz8QfXQu2IEv8eHPSFqRqqwDC3mAufd065nueHzMIY7Z5Mv04n+p/6/G4yQP0BjHAA1ulO0akfiacBl4cn/Gh/G83zzPmz/Pw+U6zTX8fHub8876Lvtgx6y/ma4xWkV/1x39/Deyl9zHYj5V8To/yX0isOksABgAA"""
    compressed_data = base64.b64decode(b64_data)
    bin_file = io.BytesIO(compressed_data)
    data = gzip.open(bin_file, 'rb').read()

    text_size = 16
    size_x, size_y = (W // 8), (H // 8)
    scale_x, scale_y = size_x / text_size, size_y / text_size

    textures = []
    RESIZE_QUALITY = 10
    for i in range(6):
        for pl in range(2):
            t = tk.PhotoImage(width=text_size, height=text_size)
            t.blank()
            d = data[i*256:(i + 1)*256]
            for p in range(text_size * text_size):
                x, y = p % text_size, p // text_size
                dp = y * 16 + x
                if d[dp] == 0:
                    t.put({0: "white", 1: "black"}.get(pl), (x, y))
            textures.append(t.zoom(round(scale_x * RESIZE_QUALITY), round(scale_y * RESIZE_QUALITY)).subsample(RESIZE_QUALITY, RESIZE_QUALITY))

    textures_promotion = []
    for i in (8, 6, 2, 4):
        for pl in range(2):
            textures_promotion.append(textures[i + pl].subsample(2))

def update_turn_display():
    turn_image.config(image=textures[g.get_turn().value-1])

def flip_view(_):
    global view
    debug_print('Flipping view', 3)
    if view == 1:
        view = 2
    elif view == 2:
        view = 1
    redraw_board()
    update_timers(g.get_clocks())


def flip_player(_):
    global player, view
    debug_print('Flipping player', 3)
    if player == 1:
        player = 2
    elif player == 2:
        player = 1


def handle_socket_recv(conn):
    debug_print('Started socket recieve handle thread')
    while running:
        ready = select([conn], [], [], 0.5)
        if not ready[0]:
            continue

        mess = conn.recv(1024)
        debug_print('Message recieved from server: {}'.format(mess), 3)

        if len(mess) == 0:
            break
        while len(mess) > 0:
            type = mess[0]
            data = mess[1:]

            if type == TYPE_OPPONENT_DISCONNECTED:
                root.after(0, lambda: reset("Opponent disconnected", True))
                mess = data

            if type == TYPE_MOVE:
                size = calcsize(MOVE_FORMAT)
                move_data = data[:size]
                mess = data[size+1:]

                plr, frox, froy, tox, toy = unpack(MOVE_FORMAT, move_data)
                fro = (frox, froy)
                to = (tox, toy)

                debug_print('Move from server: {} {} {}'.format(fro, to, plr), 3)

                move(plr, fro, to)

            if type == TYPE_PROMOTE:
                size = calcsize(PROMOTE_FORMAT)
                prom_data = data[:size]
                mess = data[size+1:]

                plr, posx, posy, piece = unpack(PROMOTE_FORMAT, prom_data)
                pos = (posx, posy)
                piece = ChessPieceType(piece)

                debug_print('Promotion from server: {} {} {}'.format(pos, piece, plr), 3)

                promote(plr, pos, piece)

            if type == TYPE_CLOCK_SYNC:
                size = calcsize(SYNC_FORMAT)
                sync_data = data[:size]
                mess = data[size+1:]

                clocks = list(unpack(SYNC_FORMAT, sync_data))

                g.sync_clock(clocks)

            if type == TYPE_GAME_END:
                root.after(0, check_game_end)
                mess = data[1:]


def player_win(player):
    print('Game won by player {}'.format(player))
    reset(str(player.name.title()) + " won")


def draw(by):
    print('Draw by', by)
    reset("Draw by {}".format(by))


def reset(mess, end=False):
    tkmsgbox.showinfo("Game end", mess)
    if end:
        root.destroy()
    else:
        g.reset()
        reset_state()
        redraw_board()


def reset_state():
    global turn, selection, selection_board, possible_moves, possible_moves_board, promotion_dialog
    selection = None
    selection_board = None
    possible_moves = None
    possible_moves_board = None
    promotion_dialog = None


if __name__ == '__main__':
    debug = 1
    custom = False
    if len(sys.argv) == 2 and sys.argv[1] == '-info':
        debug = 2
    if len(sys.argv) == 2 and sys.argv[1] == '-debug':
        debug = 3
    if len(sys.argv) == 2 and sys.argv[1] == '-custom':
        debug = 3
        custom = True

    debug_print('Running client in debug level {}'.format(debug))

    running = True

    g = ChessGame()
    g.set_clock_callback(update_timers)
    reset_state()

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serv = (input('Server address: '), 38519)
    sock.connect(serv)

    debug_print('Connected to server.')
    debug_print('Sending welcome.', 3)
    sock.send(pack(TYPE_FORMAT, TYPE_HANDSHAKE) + WELCOME_DATA)

    if custom:
        debug_print('Requesting custom game', 3)
        sock.send(pack(TYPE_FORMAT, TYPE_CUSTOM))

    pref_plr = input('Preferred pieces (white/black/either): ')
    pref_plr = pref_plr.lower()
    if pref_plr == 'w' or pref_plr == 'white':
        pref_plr = 1
    elif pref_plr == 'b' or pref_plr == 'black':
        pref_plr = 2
    elif pref_plr == 'e' or pref_plr == 'either':
        pref_plr = 0
    else:
        print('Incorrect piece preference')
        quit()

    clock = input("Game length format (mins): ")
    try:
        clock = float(clock) * 60
        clock = int(clock)
    except:
        print('Incorrect game length format')
        quit()
    config = [clock]

    debug_print('Sending preferred game config', 3)
    sock.send(pack(TYPE_FORMAT, TYPE_CONFIG) + pack(CONFIG_FORMAT, pref_plr, *config))

    if not custom:
        print("Waiting for opponent...")

    data = sock.recv(1024)
    if not custom:
        if data[0] != TYPE_OPPONENT_FOUND or data[1] != TYPE_CONFIG:
            print('Server error')
            quit()
        data = data[2:]
    if custom:
        if data[0] != TYPE_CONFIG:
            print('Server error')
            quit()
        data = data[1:]

    if not custom:
        print('Opponent found.')

    player = unpack(CONFIG_FORMAT, data)[0]
    view = player
    debug_print('Recieved player info: {}'.format(player))

    handle_enemy_moves = Thread(target=handle_socket_recv, args=(sock,))

    W, H = 720, 720
    EW = 320

    root = tk.Tk()
    root.resizable(0, 0)
    root.bind('V', flip_view)
    if debug > 1:
        root.bind('P', flip_player)
    root.geometry('{}x{}'.format(W + EW, H))
    debug_print('Created tkinter window')

    canvas = tk.Canvas(root)
    canvas.pack(fill='both', expand='yes')
    canvas.bind('<Button-1>', handle_click)
    debug_print('Created tkinter canvas')

    font = tkfont.Font(family="Arial", size=32)

    time_var1 = tk.StringVar()
    tk.Label(root, textvariable=time_var1, font=font).place(x=W+EW/2, y=H/4, anchor="center")

    time_var2 = tk.StringVar()
    tk.Label(root, textvariable=time_var2, font=font).place(x=W+EW/2, y=H*3/4, anchor="center")

    turn_image = tk.Label(root, bg='gray')
    turn_image.place(x=W+EW/2, y=H/2, anchor="center")

    handle_enemy_moves.start()

    debug_print('Initializing textures')
    initialize_textures()

    redraw_board()
    update_turn_display()

    debug_print('Starting game')

    debug_print('Starting main loop')
    root.mainloop()

    debug_print('Cleaning up')
    running = False
    handle_enemy_moves.join()
    sock.close()
