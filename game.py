from enum import Enum
from collections import namedtuple
from threading import Thread, Lock
from time import sleep

ChessPiece = namedtuple('chess_piece', ['piece', 'player'])


class ChessPieceType(Enum):
    NONE = 0
    PAWN = 1
    BISHOP = 2
    KNIGHT = 3
    ROOK = 4
    QUEEN = 5
    KING = 6


class ChessPlayer(Enum):
    NONE = 0
    WHITE = 1
    BLACK = 2

    def enemy(self):
        if self.value == 1:
            return ChessPlayer(2)
        elif self.value == 2:
            return ChessPlayer(1)
        else:
            return ChessPlayer(0)


PIECE_EMPTY = ChessPiece(ChessPieceType.NONE, ChessPlayer.NONE)


class ChessGame:
    def __init__(self):
        self.clock_callback = None
        self.reset()

    def reset(self):
        self.board = [[PIECE_EMPTY
                       for _ in range(8)] for _ in range(8)]
        home_row = [ChessPieceType.ROOK, ChessPieceType.KNIGHT,
                    ChessPieceType.BISHOP, ChessPieceType.QUEEN,
                    ChessPieceType.KING, ChessPieceType.BISHOP,
                    ChessPieceType.KNIGHT, ChessPieceType.ROOK]
        self.board[0] = [ChessPiece(pc, ChessPlayer.WHITE) for pc in home_row]
        self.board[1] = [ChessPiece(ChessPieceType.PAWN, ChessPlayer.WHITE) for _ in range(8)]
        self.board[6] = [ChessPiece(ChessPieceType.PAWN, ChessPlayer.BLACK) for _ in range(8)]
        self.board[7] = [ChessPiece(pc, ChessPlayer.BLACK) for pc in home_row]

        self.castle_available = [[True, True], [True, True]]

        self.last_move = None
        self.fifty_move_rule = 0

        self.win = None
        self.draw = None

        self.turn_lock = Lock()
        self.turn = ChessPlayer.WHITE
        self.clock_lock = Lock()
        self.clocks = [0, 0]
        self.clock_running = False
        self.running = True

    def start(self, clock):
        self.clocks = [clock, clock]
        Thread(target=self._start_clock).start()

    def stop(self):
        self.running = False

    def set_clock_callback(self, fun):
        self.clock_callback = fun

    def _start_clock(self):
        self.clock_running = True
        while self.clock_running and self.running and self.win is None:
            with self.clock_lock:
                pl = self.get_turn()
                player = pl.value - 1
                self.clocks[player] -= 1
                self._check_clock_win()
                if self.clock_callback is not None:
                    self.clock_callback(self._unsafe_get_clocks())
            sleep(1)
        self.clock_running = False

    def sync_clock(self, clocks):
        with self.clock_lock:
            self.clocks = clocks
            self._check_clock_win()
            if self.clock_callback is not None:
                self.clock_callback(self._unsafe_get_clocks())

    def _check_clock_win(self):
        for i in (1, 2):
            if self.clocks[i - 1] <= 0:
                self.win = ChessPlayer(i).enemy()

    def can_move(self, plr, fro, to):
        """
        plr - numeric player as in ChessPlayer
        fro - from coordinate tuple
        to - to coordinate tuple
        """
        if not self.running:
            return False
        from_pc = self.board[fro[0]][fro[1]]
        player = ChessPlayer(plr)

        with self.turn_lock:
            if player is not self.turn:
                return False

        # no piece
        if from_pc.piece is ChessPieceType.NONE:
            return False
        # not own piece
        if player is not from_pc.player:
            return False
        # cannot move there
        if to not in self.get_possible_moves(plr, fro):
            return False

        return True

    def move(self, plr, fro, to):
        if not self.can_move(plr, fro, to):
            return

        self._update_castle(plr, fro, to)
        self._update_en_passant(plr, fro, to)

        self._switch_turn()
        self._move(fro, to)

        self._update_fifty_move_rule(plr, fro, to)
        self._update_insufficient_material(plr, fro, to)

        self._check_win(plr)

        self.last_move = (fro, to, plr)

    def _switch_turn(self):
        with self.turn_lock:
            self.turn = self.turn.enemy()

    def can_promote(self, plr, pos):
        if self._is_in_check(plr):
            return False
        pc = self.board[pos[0]][pos[1]]
        er = {ChessPlayer.WHITE: 7, ChessPlayer.BLACK: 0}.get(ChessPlayer(plr))
        return pc.piece is ChessPieceType.PAWN and pos[0] == er

    def promote(self, plr, pos, piece):
        if not self.can_promote(plr, pos):
            return

        self.board[pos[0]][pos[1]] = ChessPiece(piece, ChessPlayer(plr))

    def _move(self, fro, to, pc=PIECE_EMPTY):
        self.board[to[0]][to[1]] = self.board[fro[0]][fro[1]]
        self.board[fro[0]][fro[1]] = pc

    def _move_reverse(self, plr, fro, to, reverse):
        if reverse is None:
            pc = self.board[to[0]][to[1]]
            self._move(fro, to)
            return pc
        else:
            self._move(to, fro, reverse)

    def _update_castle(self, plr, fro, to):
        if plr == 0:
            return
        from_pc = self.board[fro[0]][fro[1]]
        if from_pc.piece is ChessPieceType.KING:
            hr = {1: 0, 2: 7}.get(plr)
            if fro == (hr, 4):
                if to == (hr, 2):
                    self._move((hr, 0), (hr, 3))
                elif to == (hr, 6):
                    self._move((hr, 7), (hr, 5))
            self.castle_available[plr-1] = [False, False]
        elif from_pc.piece is ChessPieceType.ROOK:
            if fro[1] == 0:
                self.castle_available[plr-1][0] = False
            elif fro[1] == 7:
                self.castle_available[plr-1][1] = False

    def _update_en_passant(self, plr, fro, to):
        if self.last_move is not None and self.board[self.last_move[1][0]][self.last_move[1][1]].piece is ChessPieceType.PAWN \
            and self.last_move[1][0] == fro[0] \
                and self.last_move[1][1] == to[1]:
            self.board[self.last_move[1][0]][self.last_move[1][1]] = PIECE_EMPTY

    def _update_fifty_move_rule(self, plr, fro, to):
        if self.board[to[0]][to[1]] != PIECE_EMPTY:
            self.fifty_move_rule = 0
        elif self.board[fro[0]][fro[1]].piece is ChessPieceType.PAWN:
            self.fifty_move_rule = 0
        else:
            self.fifty_move_rule += 1
            if self.fifty_move_rule >= 50:
                self.draw = "the fifty move rule"

    def _update_insufficient_material(self, plr, fro, to):
        for p in (1, 2):
            mat = self._get_material(p)
            if not (mat[0] == mat[1] == mat[4] == 0) or not ((mat[2] + mat[3]) < 2):
                return
        self.draw = "insufficient material"

    def _get_material(self, plr):
        queen = rook = bishop = knight = pawn = 0
        for i in range(8):
            for j in range(8):
                tile = self.board[i][j]
                if tile.player != ChessPlayer(plr):
                    continue
                pc = tile.piece
                if pc is ChessPieceType.QUEEN:
                    queen += 1
                elif pc is ChessPieceType.ROOK:
                    rook += 1
                elif pc is ChessPieceType.BISHOP:
                    bishop += 1
                elif pc is ChessPieceType.KNIGHT:
                    knight += 1
                elif pc is ChessPieceType.PAWN:
                    pawn += 1
        return (queen, rook, bishop, knight, pawn)

    def get_possible_moves(self, plr, fro):
        """
        plr - numeric player as in ChessPlayer
        fro - from coordinate tuple
        """
        if not self.running:
            return []
        # results in check
        mvs_not_check = []
        for m in self._get_possible_moves(plr, fro):
            if not self._is_in_check_after(plr, fro, m):
                mvs_not_check.append(m)
        return mvs_not_check

    def _get_possible_moves(self, plr, fro):
        pc = self.board[fro[0]][fro[1]].piece
        moves = eval('self._get_possible_moves_{}(plr, fro)'.format(pc.name.lower()))
        return moves

    def _get_possible_moves_none(self, plr, foo):
        return []

    def _get_possible_moves_pawn(self, plr, fro):
        moves = []
        dir = {ChessPlayer.WHITE: 1, ChessPlayer.BLACK: -1}.get(ChessPlayer(plr))
        new = (fro[0] + dir, fro[1])
        if self._is_move_possible(plr, new) and self.board[new[0]][new[1]].player is ChessPlayer.NONE:
            moves.append(new)
            new = (fro[0] + 2 * dir, fro[1])
            if (plr == 1 and fro[0] == 1 or plr == 2 and fro[0] == 6) and self._is_move_possible(plr, new) \
               and self.board[new[0]][new[1]].player is ChessPlayer.NONE:
                moves.append(new)
        moves.extend([capt for capt in self._get_possible_check_pawn(plr, fro) if self.board[capt[0]][capt[1]].player is ChessPlayer(plr).enemy()])
        if self.last_move is not None:
            fr = self.last_move[0][0]
            dr, c = self.last_move[1]
            p = self.last_move[2]
            # pawn double move from home row
            if self.board[dr][c].piece is ChessPieceType.PAWN and ((p == 1 and fr == 1 and dr == 3) or (p == 2 and fr == 6 and dr == 4)):
                # next to this pawn
                if dr == fro[0] and (c == fro[1] - 1 or c == fro[1] + 1):
                    # en passant
                    moves.append((dr + dir, c))
        return moves

    def _get_possible_moves_knight(self, plr, fro):
        moves = []
        dirs = ((-2, -1), (-2, +1), (+2, -1), (+2, +1), (-1, -2), (+1, -2), (-1, +2), (+1, +2))
        for dx, dy in dirs:
            new = (fro[0] + dx, fro[1] + dy)
            if self._is_move_possible(plr, new):
                moves.append(new)
        return moves

    def _get_possible_moves_directional(self, plr, fro, dirs, limit=-1):
        moves = []
        plr = ChessPlayer(plr)

        for dx, dy in dirs:
            x, y = fro
            m = limit
            while self._is_move_possible(plr, (x + dx, y + dy)) == 1 and m != 0:
                x += dx
                y += dy
                m -= 1
                moves.append((x, y))
            if self._is_move_possible(plr, (x + dx, y + dy)) == 2 and m != 0:
                moves.append((x + dx, y + dy))

        return moves

    def _get_possible_moves_bishop(self, plr, fro):
        return self._get_possible_moves_directional(plr, fro, [(-1, -1), (-1, +1), (+1, -1), (+1, +1)])

    def _get_possible_moves_rook(self, plr, fro):
        return self._get_possible_moves_directional(plr, fro, [(-1, 0), (0, +1), (0, -1), (+1, 0)])

    def _get_possible_moves_queen(self, plr, fro):
        return self._get_possible_moves_directional(plr, fro, [(-1, -1), (-1, +1), (+1, -1), (+1, +1), (-1, 0), (0, +1), (0, -1), (+1, 0)])

    def _get_possible_moves_king(self, plr, fro):
        moves = self._get_possible_check_king(plr, fro)
        moves.extend(self._get_possible_castle_moves(plr))
        return moves

    def _get_possible_check_king(self, plr, fro):
        moves = []
        dirs = ((-1, -1), (-1, +1), (+1, -1), (+1, +1), (-1, 0), (0, +1), (0, -1), (+1, 0))
        for dx, dy in dirs:
            new = (fro[0] + dx, fro[1] + dy)
            if self._is_move_possible(plr, new):
                moves.append(new)
        return moves

    def _get_possible_castle_moves(self, plr):
        if plr == 0:
            return []
        moves = []
        hr = {1: 0, 2: 7}.get(plr)

        def can_move_through(plr, to):
            return self._is_move_possible(plr, to) == 1 and not self._is_in_check(plr, to)
        if self.castle_available[plr-1][0]:
            if can_move_through(plr, (hr, 3)) \
              and can_move_through(plr, (hr, 2)):
                moves.append((hr, 2))
        if self.castle_available[plr-1][1]:
            if can_move_through(plr, (hr, 5)) \
              and can_move_through(plr, (hr, 6)):
                moves.append((hr, 6))
        return moves

    def _is_move_possible(self, plr, to):
        x, y = to
        # out of board
        if x < 0 or x >= 8 or y < 0 or y >= 8:
            return 0

        # on own piece
        if self.board[x][y].player is ChessPlayer(plr):
            return 0
        # on enemy piece
        elif self.board[x][y].player is not ChessPlayer.NONE:
            return 2

        return 1

    def _get_possible_check(self, plr, fro):
        pc = self.board[fro[0]][fro[1]].piece
        if pc is ChessPieceType.PAWN:
            return self._get_possible_check_pawn(plr, fro)
        elif pc is ChessPieceType.KING:
            return self._get_possible_check_king(plr, fro)
        else:
            return self._get_possible_moves(plr, fro)

    def _get_possible_check_pawn(self, plr, fro):
        moves = []
        dr = {ChessPlayer.WHITE: 1, ChessPlayer.BLACK: -1}.get(ChessPlayer(plr))
        for dy in (-1, 1):
            new = (fro[0] + dr, fro[1] + dy)
            if self._is_move_possible(plr, new):
                moves.append(new)
        return moves

    def _is_in_check(self, plr, pos=None):
        enm = ChessPlayer(plr).enemy().value
        enm_pcs = [(x, y) for x in range(8) for y in range(8) if self.board[x][y].player.value == enm]
        enm_checks = sum([self._get_possible_check(enm, pc_pos) for pc_pos in enm_pcs], [])

        if pos is None:
            pos = self._find_board(ChessPlayer(plr), ChessPieceType.KING)
        return pos in enm_checks

    def _is_in_check_after(self, plr, fro, to):
        pc = self._move_reverse(plr, fro, to, None)
        c = self._is_in_check(plr)
        self._move_reverse(plr, fro, to, pc)
        return c

    def _check_win(self, plr):
        enm = ChessPlayer(plr).enemy().value
        enm_pcs = [(x, y) for x in range(8) for y in range(8) if self.board[x][y].player.value == enm]
        enm_mvs = sum([self.get_possible_moves(enm, pc_pos) for pc_pos in enm_pcs], [])

        check = self._is_in_check(enm)
        lmvs = len(enm_mvs)
        if lmvs == 0:
            self.running = False
            if check:
                self.win = ChessPlayer(plr)
            else:
                self.draw = "stalemate"

    def get_player_win(self):
        return self.win

    def get_draw(self):
        return self.draw

    def _unsafe_get_clocks(self):
        def transform_time(t):
            return "{:02}:{:02}".format(t // 60, t % 60)
        return tuple(map(transform_time, self.clocks))

    def get_clocks(self):
        with self.clock_lock:
            return _unsafe_get_clocks()

    def get_clocks_num(self):
        with self.clock_lock:
            return self.clocks

    def get_turn(self):
        with self.turn_lock:
            return self.turn

    def _find_board(self, plr, pc):
        for x in range(8):
            for y in range(8):
                b = self.board[x][y]
                if b.player is plr and b.piece is pc:
                    return (x, y)

    def _print_board(self):
        for y in range(7, -1, -1):
            for x in range(8):
                print(self.board[y][x][0].value, end="")
            print()
