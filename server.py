from game import ChessGame, ChessPieceType
import socket
from threading import Thread
import sys
from struct import pack, unpack, calcsize

TYPE_FORMAT = "B"
TYPE_HANDSHAKE = 72
TYPE_CONFIG = 67
TYPE_MOVE = 77
TYPE_PROMOTE = 80
TYPE_OPPONENT_FOUND = 70
TYPE_OPPONENT_DISCONNECTED = 68
TYPE_CLOCK_SYNC = 83
TYPE_GAME_END = 69
TYPE_CUSTOM = 79

PLAYER_FORMAT = "B"
POS_FORMAT = "BB"
PIECE_FORMAT = "B"
CLOCK_FORMAT = "H"
MOVE_FORMAT = PLAYER_FORMAT + POS_FORMAT + POS_FORMAT
PROMOTE_FORMAT = PLAYER_FORMAT + POS_FORMAT + PIECE_FORMAT
CONFIG_FORMAT = PLAYER_FORMAT + CLOCK_FORMAT
SYNC_FORMAT = CLOCK_FORMAT * 2

WELCOME_DATA = b"Chess connect"


def debug_print(s):
    if debug:
        print(s)


class Game:
    def __init__(self, game, sock1, sock2, config):
        self.game = game
        self.sock1 = sock1
        self.sock2 = sock2
        self.config = config


if __name__ == "__main__":
    debug = True
    if len(sys.argv) == 2 and (sys.argv[1] == '-quiet' or sys.argv[1] == '-q'):
        debug = False

    games = []
    waiting = {}


def handle(request, ip):
    global games, players
    data = request.recv(1024)

    def handle_debug_print(s, terminate=False):
        debug_print(str(ip) + ": " + s)
        if terminate:
            request.close()

    handle_debug_print("Incoming connection")

    if len(data) > 0 and data[0] != TYPE_HANDSHAKE:
        handle_debug_print("Handshake failed", True)
        return
    data = data[1:]

    if len(data) > 0 and data[:len(WELCOME_DATA)] != WELCOME_DATA:
        handle_debug_print("Handshake failed", True)
        return

    data = request.recv(1024) if len(data) == len(WELCOME_DATA) else data[len(WELCOME_DATA):]

    if len(data) == 0:
        handle_debug_print("Unexpcted end of stream", True)
        return

    custom = False
    if data[0] == TYPE_CUSTOM:
        custom = True
        data = request.recv(1024) if len(data) == 1 else data[1:]

    if len(data) == 0 or data[0] != TYPE_CONFIG:
        handle_debug_print("Config failed", True)
        return

    data = data[1:]

    config_data = unpack(CONFIG_FORMAT, data[:calcsize(CONFIG_FORMAT)])
    pref_plr = config_data[0]
    config = config_data[1:]
    handle_debug_print("Preffered player: " + str(pref_plr) + " Config: " + str(config))

    if not custom:
        this = (request, ip, pref_plr)
        if not config in waiting:
            waiting[config] = []
        waiting[config].append(this)
        handle_debug_print("Placed into waiting queue")

        arrange_game()
    if custom:
        handle_debug_print("Handling custom game")
        white = request if pref_plr == 1 else None
        black = request if pref_plr == 2 else None
        game_num, g = create_game(white, black, config)
        if pref_plr == 0: pref_plr = 1
        request.send(pack(TYPE_FORMAT, TYPE_CONFIG) + pack(CONFIG_FORMAT, pref_plr, *config))
        Thread(target=handle_requests, args=(game_num, pref_plr, request, True)).start()

def create_game(white, black, config):
    game_num = len(games)
    cg = ChessGame()
    cg.set_clock_callback(lambda clocks,game_num=game_num: handle_clock_update(game_num))
    g = Game(cg, white, black, config)
    games.append(g)
    return (game_num, cg)

def arrange_game():
    for config, pls in waiting.items():
        if len(pls) < 2:
            return

        def arrange_debug_print(s):
            debug_print("Arranging game: " + s)

        p1, p2 = pls.pop(0), pls.pop(0)
        p1sock, p1ip, p1pref = p1
        p2sock, p2ip, p2pref = p2

        white, black = None, None
        if p1pref == 1 and p2pref != 1:
            white = p1sock
        if p2pref == 1 and p1pref != 1:
            white = p2sock
        if p1pref == 2 and p2pref != 2:
            black = p1sock
        if p2pref == 2 and p1pref != 2:
            black = p2sock

        if white is None and black is None:
            white, black = p1sock, p2sock
        elif white is None and black is not None:
            white = p2sock if black == p1sock else p1sock
        elif white is not None and black is None:
            black = p2sock if white == p1sock else p1sock

        arrange_debug_print("Assigned white to " + str(p1ip) if white == p1sock else str(p2ip))
        arrange_debug_print("Assigned black to " + str(p1ip) if black == p1sock else str(p2ip))

        game_num, g = create_game(white, black, config)
        arrange_debug_print("Created game object")

        white.send(pack(TYPE_FORMAT, TYPE_OPPONENT_FOUND) + pack(TYPE_FORMAT, TYPE_CONFIG) + pack(CONFIG_FORMAT, 1, *config))
        black.send(pack(TYPE_FORMAT, TYPE_OPPONENT_FOUND) + pack(TYPE_FORMAT, TYPE_CONFIG) + pack(CONFIG_FORMAT, 2, *config))

        arrange_debug_print("Starting handling request threads")
        Thread(target=handle_requests, args=(game_num, 1, white, False)).start()
        Thread(target=handle_requests, args=(game_num, 2, black, False)).start()


def handle_requests(game_num, player_num, sock, custom):
    def handle_debug_print(s):
        debug_print("Request handler loop for player {} in {}game {}: ".format(player_num, "custom " if custom else "", game_num) + s)

    handle_debug_print("Starting handling")

    while True:
        try:
            req = sock.recv(1024)
        except:
            break
        if len(req) == 0:
            break
        while len(req) > 0:
            type = req[0]
            req = handle_request(game_num, player_num, type, req[1:], custom)

    handle_debug_print("Client disconnected")
    sock.close()
    if player_num == 1:
        games[game_num].sock1 = None
    elif player_num == 2:
        games[game_num].sock2 = None

    send_message(game_num, pack(TYPE_FORMAT, TYPE_OPPONENT_DISCONNECTED))

    if games[game_num].sock1 is games[game_num].sock2 is None:
        games[game_num].game.stop()


def handle_request(game_num, player_num, type, data, custom):
    def request_debug_print(s):
        debug_print("Handling request for player {} in {}game {}: ".format(player_num, "custom " if custom else "", game_num) + s)

    request_debug_print("Data " + str(data) + " of type " + str(type))
    game = games[game_num].game
    if type == TYPE_MOVE:
        size = calcsize(MOVE_FORMAT)
        move = data[:size]
        plr, frox, froy, tox, toy = unpack(MOVE_FORMAT, move)
        request_debug_print("Move of player {} from {},{} to {},{}".format(plr, frox, froy, tox, toy))
        if plr == player_num and game.can_move(plr, (frox, froy), (tox, toy)) or custom:
            request_debug_print("Move correct")
            if not game.clock_running:
                game.start(*games[game_num].config)
            game.move(plr, (frox, froy), (tox, toy))
            check_game_end(game_num)
            send_message(game_num, pack(TYPE_FORMAT, TYPE_MOVE)+move)
        else:
            request_debug_print("Move incorrect")
        return data[size+1:]
    if type == TYPE_PROMOTE:
        size = calcsize(PROMOTE_FORMAT)
        prom = data[:size]
        plr, posx, posy, piece = unpack(PROMOTE_FORMAT, prom)
        request_debug_print("Promotion of player {} at {},{} to piece {}".format(plr, posx, posy, piece))
        if plr == player_num and game.can_promote(plr, (posx, posy)) or custom:
            request_debug_print("Promotion correct")
            game.promote(plr, (posx, posy), ChessPieceType(piece))
            send_message(game_num, pack(TYPE_FORMAT, TYPE_PROMOTE)+prom)
        else:
            request_debug_print("Promotion incorrect")
        return data[size+1:]
    return b''

def handle_clock_update(game_num):
    g = games[game_num]
    clocks = g.game.clocks # unsafe access is ok because this callback is running within a mutex from clock thread
    send_message(game_num, pack(TYPE_FORMAT, TYPE_CLOCK_SYNC) + pack(SYNC_FORMAT, *clocks))

    check_game_end(game_num)


def check_game_end(game_num):
    def gameend_debug_print(s):
        debug_print("Checking game end for game {}: ".format(game_num) + s)
    game = games[game_num].game
    if game.get_player_win() is not None or game.get_draw() is not None:
        gameend_debug_print("Game end. Resetting")
        send_message(game_num, pack(TYPE_FORMAT, TYPE_GAME_END))
        game.reset()


def send_message(game_num, mess):
    game = games[game_num]
    if game.sock1 is not None:
        game.sock1.send(mess)
    if game.sock2 is not None:
        game.sock2.send(mess)


if __name__ == "__main__":
    HOST, PORT = '', 38519
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.bind((HOST, PORT))
    server_sock.listen(10)

    while True:
        try:
            conn, addr = server_sock.accept()
        except:
            debug_print("Received interrupt")
            break
        Thread(target=handle, args=(conn, addr,)).start()

    for clients in waiting.values():
        for conn, addr, pref_plr in clients:
            debug_print("Closing connection: " + str(addr))
            conn.close()

    debug_print("Killing server")
    server_sock.shutdown(socket.SHUT_RDWR)
    server_sock.close()
